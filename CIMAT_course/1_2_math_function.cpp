#include <iostream>
#include <cmath>
#include "../utils/utils.hpp"

using namespace std;

float calculate(float x[], float y[], int N)
{
	float numerator = 0;
	float xDenominator = 0;
	float yDenominator = 0;

	for (int i = 0; i < N; ++i)
	{
		numerator += x[i] * y[i];
		xDenominator += x[i] * x[i];
		yDenominator += y[i] * y[i];
	}

	xDenominator = sqrt(xDenominator);
	yDenominator = sqrt(yDenominator);

	if (xDenominator == 0 || yDenominator == 0)
		return 0;

	return numerator / (xDenominator * yDenominator);
}

int main(int argc, char const *argv[])
{
	int N = atoi(argv[1]);
	float x[N];
	float y[N];

	int currentArgIndex = 2;
	for (int i = 0; i < N; ++i, currentArgIndex++)
	{
		x[i] = atof(argv[currentArgIndex]);
		y[i] = atof(argv[currentArgIndex + N]);
	}

	print(x, N);
	print(y, N);

	float result = calculate(x, y, N);

	cout << "Result: " << result << endl;
	cout << "Result: " << result << endl;
	cout << "Result: " << result << endl;

	return 0;
}