#include <iostream>
#include "../utils/utils.hpp"

using namespace std;

void calculateNumbers(int* matrix[], int& nRows, int& nCols)
{
	int orderedRows = 0;
	int orderedCols = 0;
	int isOrdered = true;

	for (int j = 0; j < nCols; ++j)
	{
		isOrdered = true;

		for (int i = 0; i < nRows - 1; ++i)
		{
			if (matrix[i][j] > matrix[i + 1][j])
			{
				isOrdered = false;
				break;
			}
		}

		if (isOrdered)
			orderedCols++;
	}

	for (int i = 0; i < nRows; ++i)
	{
		isOrdered = true;

		for (int j = 0; j < nCols - 1; ++j)
		{
			if (matrix[i][j] > matrix[i][j + 1])
			{
				isOrdered = false;
				break;
			}
		}

		if (isOrdered)
			orderedRows++;
	}

	nRows = orderedRows;
	nCols = orderedCols;
}

int main(int argc, char const *argv[])
{
	int nRows = atoi(argv[1]);
	int nCols = atoi(argv[2]);

	int* matrix[nRows];
	createMatrix(matrix, nRows, nCols);

	int currentArgIndex = 3;

	for (int i = 0; i < nRows; ++i)
		for (int j = 0; j < nCols; ++j, currentArgIndex++)
			matrix[i][j] = atoi(argv[currentArgIndex]);

	print(matrix, nRows, nCols);

	calculateNumbers(matrix, nRows, nCols);

	cout << "Cols: " << nCols << endl;
	cout << "Rows: " << nRows << endl;

	return 0;
}