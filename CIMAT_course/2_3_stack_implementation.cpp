#include <iostream>
#include "../utils/utils.hpp"

using namespace std;

const int N = 15;

template <class T>
struct StackInefficient
{
	T array[N];
	unsigned int nElements;

	StackInefficient()
	{
		nElements = 0;
	}

	int size()
	{
		return nElements;
	}

	bool isEmpty()
	{
		return nElements < 1;
	}

	void push(T element)
	{
		if (nElements >= N)
			throw "The stack is full";

		array[nElements++] = element;
	}

	T pop()
	{
		if (isEmpty())
			throw "The stack is empty";

		return array[--nElements];
	}

	T minimum()
	{
		if (isEmpty())
			throw "The stack is empty";

		T min = array[0];

		for (int i = 1; i < nElements; ++i)
		{
			if (array[i] < min)
				min = array[i];
		}

		return min;
	}

	void printValues()
	{
		print(array, N);
	}
};

template <class T>
struct StackEfficient
{
	T array[N];
	T minima[N];
	unsigned int nElements;

	StackEfficient()
	{
		nElements = 0;
	}

	int size()
	{
		return nElements;
	}

	bool isEmpty()
	{
		return nElements < 1;
	}

	void push(T element)
	{
		if (nElements >= N)
			throw "The stack is full";

		array[nElements] = element;

		if (isEmpty())
		{
			minima[0] = element;
		}
		else
		{
			minima[nElements] = element < minima[nElements - 1] ?
								element :
								minima[nElements - 1];
		}

		nElements++;
	}

	T pop()
	{
		if (isEmpty())
			throw "The stack is empty";

		return array[--nElements];
	}

	T minimum()
	{
		return minima[nElements - 1];
	}

	void printValues()
	{
		print(array, nElements);
	}
};

template <class T>
struct StackBest
{
	T array[N];
	T currentMininum;
	unsigned int nElements;

	StackBest()
	{
		nElements = 0;
	}

	int size()
	{
		return nElements;
	}

	bool isEmpty()
	{
		return nElements < 1;
	}

	void push(T element)
	{
		if (nElements >= N)
			throw "The stack is full";


		if (isEmpty())
		{
			currentMininum = element;
		}
		else if (element < currentMininum)
		{
			int temp = element;
			element = 2 * element - currentMininum;
			currentMininum = temp;
		}

		array[nElements++] = element;
	}

	T pop()
	{
		if (isEmpty())
			throw "The stack is empty";

		if (array[nElements - 1] < currentMininum)
		{
			int temp = currentMininum;
			currentMininum = 2 * currentMininum - array[nElements - 1];
			array[nElements - 1] = temp;
		}

		return array[--nElements];
	}

	T minimum()
	{
		return currentMininum;
	}

	void printValues()
	{
		print(array, nElements);
	}
};

int main(int argc, char const *argv[])
{
	int N = atoi(argv[1]);
	int currentArgsIndex = 2;
	StackBest<int> stack;

	for (int i = 0; i < N; ++i)
		stack.push(atoi(argv[currentArgsIndex++]));

	stack.printValues();

	while (!stack.isEmpty())
	{
		cout << "Minimum: " << stack.minimum() << endl;
		cout << "Poped: " << stack.pop() << endl;
	}


	return 0;
}