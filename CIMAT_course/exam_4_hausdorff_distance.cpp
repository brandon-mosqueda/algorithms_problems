#include <iostream>
#include <cmath>
#include <cfloat>
#include "../utils/utils.hpp"

using namespace std;

struct Point
{
	float x;
	float y;
};

void printPoints(Point array[], int length)
{
	for (int i = 0; i < length; ++i)
		cout << "(" << array[i].x << ", " << array[i].y << "), ";

	cout << endl;
}

float getEuclideanDistance(Point point, Point point2)
{
	return sqrt(pow(point2.x - point.x, 2) + pow(point2.y - point.y, 2));
}

float getHDistance(Point X[], int nX, Point Y[], int nY)
{
	float hDistance = -1;

	for (int iX = 0; iX < nX; ++iX)
	{
		float minEuclideanDistance = FLT_MAX;

		for (int iY = 0; iY < nY; ++iY)
		{
			float euclideanDistance = getEuclideanDistance(X[iX], Y[iY]);

			minEuclideanDistance = min(minEuclideanDistance, euclideanDistance);
		}

		hDistance = max(hDistance, minEuclideanDistance);
	}

	return hDistance;
}

float getHausdorffDistance(Point X[], int nX, Point Y[], int nY)
{
	float xyDistance = getHDistance(X, nX, Y, nY);
	float yxDistance = getHDistance(Y, nY, X, nX);

	return max(xyDistance, yxDistance);
}

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		cout << "Case: " << nTestCase << endl;

		int nX = atoi(argv[currentArgIndex++]);
		int nY = atoi(argv[currentArgIndex++]);
		Point X[nX];
		Point Y[nY];

		for (int i = 0; i < nX; ++i)
		{
			Point point
			{
				.x = (float)atof(argv[currentArgIndex++]),
				.y = (float)atof(argv[currentArgIndex++])
			};

			X[i] = point;
		}

		for (int i = 0; i < nY; ++i)
		{
			Point point
			{
				.x = (float)atof(argv[currentArgIndex++]),
				.y = (float)atof(argv[currentArgIndex++])
			};

			Y[i] = point;
		}

		cout << "X: "; printPoints(X, nX);
		cout << "Y: "; printPoints(Y, nY);
		cout << "HausdorffDistance: " << getHausdorffDistance(X, nX, Y, nY) << endl;

		cout << "*********" << endl << endl << endl;
	}

	return 0;
}