#include <iostream>
#include "../utils/utils.hpp"

using namespace std;

void getSumDiagonals(int* matrix[], int nRows, int nCols, int diagonalsSums[])
{
	int nTotalDiagonals = nCols == 1 ? nRows : nRows + 1;
	int currentCol = nCols - 1;
	int currentRow = 0;

	for (int iDiagonal = 0; iDiagonal < nTotalDiagonals; ++iDiagonal)
	{
		int diagonalSum = 0;
		int i = currentRow;
		int j = currentCol;

		while (i < nRows && j < nCols)
			diagonalSum += matrix[i++][j++];

		if (currentCol > 0)
			currentCol--;
		else
			currentRow++;

		diagonalsSums[iDiagonal] = diagonalSum;
	}
}

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		cout << "Case: " << nTestCase << endl;

		int nRows = atoi(argv[currentArgIndex++]);
		int nCols = atoi(argv[currentArgIndex++]);
		int* matrix[nRows];
		createMatrix(matrix, nRows, nCols);

		for (int i = 0; i < nRows; ++i)
			for (int j = 0; j < nCols; ++j)
				matrix[i][j] = atoi(argv[currentArgIndex++]);

		print(matrix, nRows, nCols);

		int diagonalsSums[nCols == 1 ? nRows : nRows + 1];

		getSumDiagonals(matrix, nRows, nCols, diagonalsSums);

		print(diagonalsSums, nCols == 1 ? nRows : nRows + 1);

		cout << "*********" << endl << endl << endl;
	}

	return 0;
}