#include <iostream>
#include <cmath>
#include "../utils/utils.hpp"

using namespace std;

float getDistance(int N, float x[], float y[])
{
	float distance = 0;

	for (int i = 0; i < N; ++i)
		distance += pow(x[i] - y[i], 2);

	return sqrt(distance);
}

void solve(int N, float epsilon, float* coefficients[],
		   float responses[], float x[])
{
	// Initial solution
	float nextSolution[N];
	for (int i = 0; i < N; ++i)
		nextSolution[i] = 0.01;

	float distance = 0;

	do {
		for (int i = 0; i < N; ++i)
			x[i] = nextSolution[i];

		float numerator = 0;

		for (int i = 0; i < N; ++i)
		{
			for (int j = 0; j < N; ++j)
			{
				numerator += -(coefficients[i][j] * x[j]);
			}

			numerator += coefficients[i][i] * x[i];

			nextSolution[i] = numerator / coefficients[i][i];
		}

		distance = getDistance(N, x, nextSolution);
		cout << "Distance: " << distance << endl;
		print(x, N);
		print(nextSolution, N);

	} while (abs(distance) >= epsilon);
}

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		int N = atoi(argv[currentArgIndex++]);
		float epsilon = atoi(argv[currentArgIndex++]);
		float* coefficients[N];
		createMatrix(coefficients, N, N);

		float responses[N];
		float x[N] = {0};

		for (int i = 0; i < N; ++i)
			for (int j = 0; j < N; ++j)
				coefficients[i][j] = atof(argv[currentArgIndex++]);

		for (int i = 0; i < N; ++i)
			responses[i] = atof(argv[currentArgIndex++]);

		cout << "Test case: " << nTestCase + 1 << endl;

		cout << "N: " << N << endl;
		cout << "epsilon: " << epsilon << endl;

		cout << "Coefficients" << endl;
		print(coefficients, N, N);

		cout << "Responses" << endl;
		print(responses, N);

		solve(N, epsilon, coefficients, responses, x);
	}

	return 0;
}