#include <iostream>
#include "../utils/utils.hpp"

using namespace std;

int merge(int array[], int temp[], int low, int mid, int high)
{
	int nInversions = 0;
	int left = low;
	int right = mid + 1;
	int i = low;

	while (left <= mid && right <= high)
	{
		if (array[left] <= array[right])
		{
			temp[i] = array[left++];
		}
		else
		{
			nInversions++;
			temp[i] = array[right++];
		}

		i++;
	}

	while (left <= mid)
	{
		nInversions++;
		temp[i++] = array[left++];
	}

	while (right <= high)
		temp[i++] = array[right++];

	for (i = low; i <= high; ++i)
		array[i] = temp[i];

	return nInversions;
}

int mergeCounts(int array[], int temp[], int low, int high)
{
	if (low >= high)
		return 0;

	int mid = (low + high) / 2;

	int leftInversions = mergeCounts(array, temp, low, mid);
	int rightInversions = mergeCounts(array, temp, mid + 1, high);

	int nInversions = leftInversions;

	nInversions += merge(array, temp, low, mid, high);

	return nInversions;
}

int coutInversions(int array[], int length)
{
	if (length == 1)
		return 0;

	int temp[length];

	return mergeCounts(array, temp, 0, length - 1);
}

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		cout << "Case: " << nTestCase << endl;
		int N = atoi(argv[currentArgIndex++]);
		int array[N];

		for (int i = 0; i < N; ++i)
			array[i] = atoi(argv[currentArgIndex++]);

		print(array, N);

		cout << "nInversions: " << coutInversions(array, N) << endl;
		print(array, N);

		cout << "***********" << endl << endl << endl;
	}

	return 0;
}