#include <iostream>
#include <cmath>
#include "../utils/utils.hpp"

using namespace std;

double getCircleArea(double radio)
{
	return M_PI * radio * radio;
}

long getCircles(double radio, long long nPaint)
{
	long nPaintedCircles = 0;
	long i = 0;

	while (nPaint > 0)
	{
		double innerCircleArea = getCircleArea(radio);
		double outerCircleArea = getCircleArea(radio + 1);

		double areaToPaint = outerCircleArea - innerCircleArea;

		nPaint -= areaToPaint;

		if (nPaint >= 0)
			nPaintedCircles++;

		// Painted and empty line
		radio += 2;
		i++;
	}

	cout << "Operations: " << i << endl;

	return nPaintedCircles;
}

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		cout << "Case: " << nTestCase << endl;
		double radio = atof(argv[currentArgIndex++]);
		long long nPaint;
		sscanf(argv[currentArgIndex++], "%lld", &nPaint);

		cout << "radio: "<< radio << endl;
		cout << "nPaint: "<< nPaint << endl;
		cout << "Circles: "<< getCircles(radio, nPaint) << endl;

		cout << "***************" << endl << endl;
	}

	return 0;
}