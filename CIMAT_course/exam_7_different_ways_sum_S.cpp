#include <iostream>
#include "../utils/utils.hpp"

using namespace std;

int countDifferentWays(int array[], int nElements, int S, int from = 0)
{
	int counter = 0;

	for (int i = from; i < nElements; ++i)
	{
		if (array[i] == S)
		{
			counter++;
		}
		else if (array[i] < S)
		{
			counter += countDifferentWays(array, nElements,
									 	  S - array[i], i + 1);
		}
	}

	return counter;
}

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		cout << "Case: " << nTestCase << endl;

		int nElements = atoi(argv[currentArgIndex++]);
		int S = atoi(argv[currentArgIndex++]);
		int array[nElements];

		for (int i = 0; i < nElements; ++i)
			array[i] = atoi(argv[currentArgIndex++]);

		print(array, nElements);

		cout << "different ways of obtain " << S << ": "
			 << countDifferentWays(array, nElements, S) << endl;

		cout << "*********" << endl << endl << endl;
	}

	return 0;
}