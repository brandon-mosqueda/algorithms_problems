#include <iostream>
#include "../utils/utils.hpp"
#include "../utils/datastructs.hpp"

using namespace std;

const int VISITED = -1;

struct Cell
{
	int row;
	int col;
	int height;
};

void visitCell(int N, int row, int col, int currentHeight, int* matrix[],
			   Stack<Cell> &stack, int &nVisitedCells)
{
	if ((col < N && col >= 0) && (row >= 0 && row < N))
	{
		int height = matrix[row][col];

		if (height != VISITED and height == currentHeight)
		{
			matrix[row][col] = VISITED;
			nVisitedCells++;

			Cell cell
			{
				.row = row,
				.col = col,
				.height = height
			};

			stack.push(cell);
		}
	}
}

int getRequiredGuardians(int N, int* matrix[])
{
	int nVisitedCells = 0;
	int nRequiredGuardians = 0;

	Stack<Cell> stack;

	for (int i = 0; i < N && nVisitedCells < N * N; i++)
	{
		for (int j = 0; j < N && nVisitedCells < N * N; j++)
		{
			if (matrix[i][j] != VISITED)
			{
				print(matrix, N, N, true);

				Cell cell
				{
					.row = i,
					.col = j,
					.height = matrix[i][j]
				};

				matrix[i][j] = VISITED;

				stack.push(cell);

				nVisitedCells++;
				nRequiredGuardians++;

				while (!stack.isEmpty())
				{
					Cell currentCell = stack.pop();

					// East
					visitCell(N, currentCell.row, currentCell.col + 1,
							  currentCell.height, matrix,
							  stack, nVisitedCells);

					// South
					visitCell(N, currentCell.row + 1, currentCell.col,
							  currentCell.height, matrix,
							  stack, nVisitedCells);
				}
			}
		}
	}

	return nRequiredGuardians;
}

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		cout << "Case: " << nTestCase << endl;
		int N = atoi(argv[currentArgIndex++]);
		int* matrix[N];
		createMatrix(matrix, N, N);

		for (int i = 0; i < N; ++i)
			for (int j = 0; j < N; j++)
				matrix[i][j] = atoi(argv[currentArgIndex++]);

		print(matrix, N, N);

		cout << "Required Guardians: " << getRequiredGuardians(N, matrix) << endl;

		print(matrix, N, N);

		cout << "*********" << endl << endl << endl;
	}

	return 0;
}