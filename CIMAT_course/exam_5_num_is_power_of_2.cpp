#include <iostream>
#include "../utils/utils.hpp"

using namespace std;

bool isPowerOfTwo(int num)
{
	if (num % 2 == 1 || num < 1)
		return false;
	if (num == 1 || num == 2)
		return true;

	bool isMultiple = false;
	int powers = 1;

	while (powers < num)
	{
		powers = powers * 2;
	}

	return powers == num;
}

bool isPowerOfTwoEfficient(int num)
{
	return ((num - 1) & num) == 0;
}

int main(int argc, char const *argv[])
{
	cout << boolalpha;

	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		cout << "Case: " << nTestCase << endl;
		int  num = atoi(argv[currentArgIndex++]);

		cout << "num: " << num << " : " << isPowerOfTwo(num) << endl;
		cout << "num: " << num << " : " << isPowerOfTwoEfficient(num) << endl;

		cout << "*********" << endl << endl << endl;
	}

	return 0;
}