#include <iostream>
#include <vector>
#include <climits>
#include "../utils/utils.hpp"
#include "../utils/datastructs.hpp"

using namespace std;

long getCapacityAugmentingPathBFS(int* capacities[],
							 	  int nNodes,
							 	  LinkedList<int> neighbors[],
							 	  int source, int sink,
							 	  int* flowMatrix[],
							 	  int augmentingPath[])
{
	const int NOT_VISITED = -1;

	for (int i = 0; i < nNodes; ++i)
		augmentingPath[i] = NOT_VISITED;
	augmentingPath[source] = -2;

	long capacityUntilNodes[nNodes] = {};
	capacityUntilNodes[source] = INT_MAX;

	Queue<int> queue;
	queue.enqueue(source);

	while (!queue.isEmpty())
	{
		int currentNode = queue.dequeue();
		LinkedList<int> neighborsNodes = neighbors[currentNode];

		for (int* i = neighborsNodes.begin();
			 i != NULL;
			 i = neighborsNodes.next())
		{
			int neighborNode = *i;
			long availableFlow = capacities[currentNode][neighborNode] -
								 flowMatrix[currentNode][neighborNode];
			if (availableFlow > 0
				&& augmentingPath[neighborNode] == NOT_VISITED)
			{
				augmentingPath[neighborNode] = currentNode;
				capacityUntilNodes[neighborNode] = min(
					capacityUntilNodes[currentNode], availableFlow
				);

				if (neighborNode != sink)
					queue.enqueue(neighborNode);
				else
					return capacityUntilNodes[sink];
			}
		}
	}

	return 0;
}

long getMaxFlowByEdmondsKarp(int* capacities[],
							 int nNodes,
							 LinkedList<int> neighbors[],
							 int source, int sink,
							 int* flowMatrix[])
{
	long maxFlow = 0;

	int* residualCapacity[nNodes] = {};
	int augmentingPath[nNodes] = {};
	createMatrix(residualCapacity, nNodes, nNodes);

	while (true)
	{
		long augmentingPathCapacity = getCapacityAugmentingPathBFS(
			capacities, nNodes, neighbors, source,
			sink, flowMatrix, augmentingPath
		);

		if (augmentingPathCapacity == 0)
			break;

		maxFlow += augmentingPathCapacity;

		int currentNode = sink;

		while (currentNode != source)
		{
			int neighborNode = augmentingPath[currentNode];
			flowMatrix[neighborNode][currentNode] += augmentingPathCapacity;
			flowMatrix[currentNode][neighborNode] -= augmentingPathCapacity;
			currentNode = neighborNode;
		}
	}

	return maxFlow;
}

char getLetter(int value);

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int argsCurrentIndex = 2;

	for (int i = 0; i < nTestCases; ++i)
	{
		int nNodes = atoi(argv[argsCurrentIndex++]);

		int* capacities[nNodes] = {};
		createMatrix(capacities, nNodes, nNodes);

		LinkedList<int> neighbors[nNodes] = {};

		int source = atoi(argv[argsCurrentIndex++]) - 1;
		int sink = atoi(argv[argsCurrentIndex++]) - 1;
		int nEdges = atoi(argv[argsCurrentIndex++]);

		cout << "Case: " << i + 1 << endl;

		for (int j = 0; j < nEdges; ++j)
		{
			int from = atoi(argv[argsCurrentIndex++]) - 1;
			int to = atoi(argv[argsCurrentIndex++]) - 1;
			int capacity = atoi(argv[argsCurrentIndex++]);

			cout << getLetter(from) << " => " << getLetter(to) << " = " << capacity << endl;

			capacities[from][to] = capacity;
			neighbors[from].push(to);
		}

		print(capacities, nNodes, nNodes);
		cout << "Source neighbors: ";
		neighbors[0].print();
		int* maxFlowPath[nNodes];
		createMatrix(maxFlowPath, nNodes, nNodes);

		long maxFlow = getMaxFlowByEdmondsKarp(capacities, nNodes,
											   neighbors, source,
											   sink, maxFlowPath);

		cout << "Max flow: " << maxFlow << endl << "***********"
			 << endl << endl;
	}

	return 0;
}

char getLetter(int value)
{
	value++;

	if (value < 1)
		return ' ';
	if (value == 1)
		return 'S';

	return (char) value + 63;
}