#include <iostream>
#include "../utils/utils.hpp"

using namespace std;

enum SolutionTypes
{
	NO_SOLUTION = 0,
	ONE_SOLUTION = 1,
	INFINITY_SOLUTIONS = 2
};

void solve(double a, double b, double c,
		   double d, double e, double f,
		   double &x, double &y, SolutionTypes &solutionType)
{
	/*
	 	A system with the form:
	 	ax + by = c
	 	dx + ey = f
	*/

	if (((a / d == b / e) && (a / d == c / f))
		|| ((d / a == e / b) && (d / a == f / c)))
	{
		x = 0;
		y = 0;
		solutionType = INFINITY_SOLUTIONS;

		return;
	}

	double discriminant = a * e - b * d;

	if (discriminant == 0)
	{
		x = 0;
		y = 0;
		solutionType = NO_SOLUTION;

		return;
	}

	discriminant = 1 / discriminant;

	cout << "discriminant: " << discriminant << endl;

	double inverse[2][2] = {
		{e * discriminant, (-b) * discriminant},
		{(-d) * discriminant, a * discriminant}
	};

	cout << inverse[0][0] << ", " << inverse[0][1] << endl;
	cout << inverse[1][0] << ", " << inverse[1][1] << endl;

	x = inverse[0][0] * c + inverse[0][1] * f;
	y = inverse[1][0] * c + inverse[1][1] * f;
	solutionType = ONE_SOLUTION;

	return;
}

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		cout << "Case: " << nTestCase << endl;

		double a = atof(argv[currentArgIndex++]);
		double b = atof(argv[currentArgIndex++]);
		double c = atof(argv[currentArgIndex++]);
		double d = atof(argv[currentArgIndex++]);
		double e = atof(argv[currentArgIndex++]);
		double f = atof(argv[currentArgIndex++]);

		double x, y;
		SolutionTypes solutionType;

		solve(a, b, c, d, e, f, x, y, solutionType);

		cout << a << "x + " << b << "y = " << c << endl;
		cout << d << "x + " << e << "y = " << f << endl;
		cout << "x = " << x << endl;
		cout << "y = " << y << endl;
		cout << "SolutionType = " << solutionType << endl;

		cout << "*********" << endl << endl << endl;
	}

	return 0;
}