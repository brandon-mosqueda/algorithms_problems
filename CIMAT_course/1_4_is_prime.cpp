#include <iostream>
#include <cmath>
#include "../utils/utils.hpp"

using namespace std;

bool isPrime(int number)
{
	if (number <= 1)
		return false;
	else if (number <= 3)
		return true;
	else if (number % 2 == 0)
		return false;

	int divisor = 3;
	int limit = sqrt(number) + 1;

	while (divisor <= limit)
	{
		if (number % divisor == 0)
			return false;

		divisor += 2;
	}

	return true;
}

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		cout << atoi(argv[currentArgIndex]) << ": "
			 << isPrime(atoi(argv[currentArgIndex++])) << endl;
	}

	return 0;
}