#include <iostream>
#include "../utils/utils.hpp"

using namespace std;

long maxSum(int array[], int N)
{
	int max = array[0];

	for (int i = 1, iPrevious = 0; i < N; ++i, iPrevious++)
	{
		if (array[iPrevious] + array[i] > array[i])
			array[i] += array[iPrevious];

		if (array[i] > max)
			max = array[i];
	}

	return max;
}

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		cout << "Case: " << nTestCase << endl;
		int N = atoi(argv[currentArgIndex++]);
		int array[N];

		for (int i = 0; i < N; ++i)
			array[i] = atoi(argv[currentArgIndex++]);

		print(array, N);

		cout << "Max sum of contiguos elements: "
			 << maxSum(array, N) << endl;

		cout << "*********" << endl << endl << endl;
	}

	return 0;
}