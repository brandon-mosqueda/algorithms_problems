#include <iostream>
#include "../utils/utils.hpp"
#include "../utils/datastructs.hpp"

using namespace std;

int getNumberOfNoVElements(int array[], int N, int v)
{
	int elementsDifferentOfV = 0;
	int lastVIndex = 0;
	Queue<int> availableIndexes;

	for (int i = 0; i < N; ++i)
	{
		if (array[i] == v)
		{
			availableIndexes.enqueue(i);
		}
		else
		{
			elementsDifferentOfV++;

			if (!availableIndexes.isEmpty())
			{
				int newIndex = availableIndexes.dequeue();
				array[newIndex] = array[i];
				availableIndexes.enqueue(i);
			}
		}
	}

	return elementsDifferentOfV;
}

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		cout << "Case: " << nTestCase + 1 << endl;

		int N = atoi(argv[currentArgIndex++]);
		int v = atoi(argv[currentArgIndex++]);
		int array[N];

		for (int i = 0; i < N; ++i, currentArgIndex++)
			array[i] = atoi(argv[currentArgIndex]);

		print(array, N);

		int result = getNumberOfNoVElements(array, N, v);

		cout << "Result: " << result << endl;

		print(array, N);

		cout << "*********" << endl << endl << endl;
	}

	return 0;
}