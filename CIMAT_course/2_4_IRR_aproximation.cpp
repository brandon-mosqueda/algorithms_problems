#include <iostream>
#include <cmath>
#include "../utils/utils.hpp"

using namespace std;

double getIRR(int N, double CF[], double tolerance=10e-3,
			 double minValue=-1, double maxValue=1e7)
{
	double result;
	double IRR;

	do {
		IRR = (maxValue - minValue) / 2 + minValue;

		result = CF[0];

		for (int i = 1; i <= N; ++i)
			result += CF[i] / pow(1.0 + IRR, i);

		if (result < 0)
			maxValue = IRR;
		else
			minValue = IRR;

	} while (abs(result) >= tolerance);

	return IRR;
}

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		cout << "Case: " << nTestCase << endl;

		int N = atoi(argv[currentArgIndex++]);
		double CF[N + 1];

		for (int i = 0; i <= N; ++i)
			sscanf(argv[currentArgIndex++], "%lf", &(CF[i]));
			// CF[i] = atof(argv[currentArgIndex++]);

		print(CF, N);

		cout << "IRR: " << getIRR(N, CF) << endl;
		cout << "*********" << endl << endl << endl;
	}

	return 0;
}