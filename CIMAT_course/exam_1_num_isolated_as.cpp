#include <iostream>
#include "../utils/utils.hpp"

using namespace std;

 int getNumIsolatedAs(string text)
{
	int length = text.length();
	int nIsolatedAs = 0;
	bool beforeWasA = false;

	for (int i = 0; i < length - 1; ++i)
	{
		if (text[i] == 'a')
		{
			if (!beforeWasA)
				if (text[i + 1] != 'a')
					nIsolatedAs++;
				else
					i++;

			beforeWasA = true;
		}
		else
		{
			beforeWasA = false;
		}
	}

	return nIsolatedAs;
}

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		cout << "Case: " << nTestCase << endl;
		string text(argv[currentArgIndex++]);

		cout << text << endl;

		cout << "getNumIsolatedAs: " << getNumIsolatedAs(text) << endl;

		cout << "*********" << endl << endl << endl;
	}

	return 0;
}