#include <iostream>
#include "../utils/utils.hpp"

using namespace std;

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		cout << "Case: " << nTestCase << endl;
		int N = atoi(argv[currentArgIndex++]);
		int array[N];

		for (int i = 0; i < N; ++i)
			array[i] = atoi(argv[currentArgIndex++]);

		print(array, N);

		cout << "*********" << endl << endl << endl;
	}

	return 0;
}