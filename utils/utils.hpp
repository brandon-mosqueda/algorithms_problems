#include <vector>

template <class T>
void print(T vec[], int length, std::string delimiter = ", ")
{
	for (int i = 0; i < length - 1; ++i)
		std::cout << vec[i] << delimiter;

	if (length > 0)
		std::cout << vec[length - 1];

	std::cout << std::endl;
}

template <class T>
void print(T* matrix, int nRows, int nCols, bool printValue=true,
		   std::string delimiter = ", ")
{
	for (int i = 0; i < nRows; ++i)
	{
		for (int j = 0; j < nCols; ++j)
		{
			printValue ? std::cout << matrix[i][j]
					   : std::cout << &(matrix[i][j]);

			std::cout << delimiter;
		}

		std::cout << std::endl;
	}

	std::cout << std::endl;
}

template <class T>
void print(std::vector<T> vec, std::string delimiter = ", ")
{
	int length = vec.size();

	for (int i = 0; i < length - 1; ++i)
		std::cout << vec[i] << delimiter;

	if (length > 0)
		std::cout << vec[length - 1];

	std::cout << std::endl;
}

template <class T>
void createMatrix(T* pointersVector[], int nRows, int nCols)
{
	for (int i = 0; i < nRows; ++i)
		pointersVector[i] = new T[nCols];
}