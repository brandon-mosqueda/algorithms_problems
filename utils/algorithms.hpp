template <class T>
void merge(T array[], T temp[], int low, int mid, int high)
{
   int left = low;
   int right = mid + 1;
   int i = low;

   while (left <= mid && right <= high)
   {
      if (array[left] <= array[right])
         temp[i] = array[left++];
      else
         temp[i] = array[right++];

      i++;
   }

   while (left <= mid)
      temp[i++] = array[left++];

   while(right <= high)
      temp[i++] = array[right++];

   for(i = low; i <= high; i++)
      array[i] = temp[i];
}

template <class T>
void mergeSort(T array[], T temp[], int low, int high)
{
   if (low >= high)
      return;

   int mid = (low + high) / 2;

   mergeSort(array, temp, low, mid);
   mergeSort(array, temp, mid + 1, high);

   merge(array, temp, low, mid, high);
}

template <class T>
void mergeSort(T array[], int length)
{
   if (length == 1)
      return;

   T temp[length];

   mergeSort(array, temp, 0, length - 1);
}