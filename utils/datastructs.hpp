template<typename T>
struct Node
{
	T value;
	struct Node<T>* next;
	struct Node<T>* previous;

	Node()
	{
		next = NULL;
		previous = NULL;
	}

	void print()
	{
		std::cout << value;
	}
};

template<typename T>
struct Queue
{
	struct Node<T>* first;
	struct Node<T>* last;

	int nElements;

	Queue()
	{
		nElements = 0;
		first = NULL;
		last = NULL;
	}

	bool isEmpty()
	{
		return nElements == 0;
	}

	void enqueue(T element)
	{
		Node<T>* node = new Node<T>;
		node->value = element;

		if (nElements == 0)
		{
			first = node;
			last = node;
		}
		else
		{
			last->next = node;
			last = node;
		}

		nElements++;
	}

	T dequeue()
	{
		if (first == NULL)
			throw "Trying to dequeue on empty queue";

		T value = first->value;

		if (nElements == 1)
		{
			first = NULL;
			last = NULL;
		}
		else
		{
			first = first->next;
		}

		nElements--;

		return value;
	}

	T front()
	{
		if (first == NULL)
			throw "There is no elements in the queue";

		return first->value;
	}

	int size()
	{
		return nElements;
	}

	void print(std::string delimiter = ", ")
	{
		Node<T>* current = first;

		while(current != NULL)
		{
			current->print();
			std::cout << delimiter;
			current = current->next;
		}

		std::cout << std::endl;
	}
};

template <typename T>
struct LinkedList
{
	struct Node<T>* first;
	struct Node<T>* last;
	// For iterations
	struct Node<T>* current;

	int nElements;

	LinkedList()
	{
		first = NULL;
		last = NULL;
		current = NULL;
		nElements = 0;
	}

	T operator[](int index)
	{
		if (index < 0 || index >= nElements)
			throw std::out_of_range(index + " not valid");

		Node<T>* temp = first;

		int i = 0;

		while (i != index)
		{
			i++;
			temp = temp->next;
		}

		return temp->value;
	}

	void push(T element)
	{
		Node<T>* node = new Node<T>;
		node->value = element;

		if (first == NULL)
		{
			first = node;
			last = node;
		}
		else
		{
			last->next = node;
			last = node;
		}

		nElements++;
	}

	int size()
	{
		return nElements;
	}

	T* begin()
	{
		if (first == NULL)
			return NULL;

		current = first;
		return &first->value;
	}

	T* next()
	{
		if (current == NULL)
			return NULL;

		current = current->next;
		return &current->value;
	}

	void print(std::string delimiter = ", ")
	{
		Node<T>* current = first;

		while(current != NULL)
		{
			current->print();
			std::cout << delimiter;
			current = current->next;
		}

		std::cout << std::endl;
	}
};

template <class T>
struct Stack
{
	Node<T>* last;
	unsigned int nElements;

	Stack()
	{
		nElements = 0;
		last = NULL;
	}

	int size()
	{
		return nElements;
	}

	bool isEmpty()
	{
		return nElements < 1;
	}

	void push(T element)
	{
		Node<T>* node = new Node<T>;
		node->value = element;

		if (last == NULL)
		{
			last = node;
		}
		else
		{
			last->next = node;
			node->previous = last;
			last = node;
		}

		nElements++;
	}

	T pop()
	{
		if (last == NULL)
			throw "Trying to pop on empty stack";

		T value = last->value;

		if (nElements == 1)
		{
			last = NULL;
		}
		else
		{
			last = last->previous;
			last->next = NULL;
		}

		nElements--;

		return value;
	}

	void print(std::string delimiter = ", ")
	{
		Node<T>* current = last;

		while(current != NULL)
		{
			current->print();
			std::cout << delimiter;
			current = current->previous;
		}

		std::cout << std::endl;
	}
};