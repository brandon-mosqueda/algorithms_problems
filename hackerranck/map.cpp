#include <iostream>
#include <string>
#include <utility>
#include <iomanip>
#include <map>

#include "../utils/utils.hpp"

using namespace std;

typedef pair<string, string> contact;

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	map<string, string> contacts;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		cout << "Case: " << nTestCase << endl;
        string name(argv[currentArgIndex++]);
        string phone(argv[currentArgIndex++]);

        contacts.insert(contact(name, phone));
        cout << name << endl;
		cout << "*********" << endl << endl << endl;
	}

	map<string, string>::iterator p = contacts.begin();

	while (p != contacts.end())
  	{
    	cout << setw(10) << p->first << setw(12) << p->second << endl;
    	p++;
	}

	cout << contacts.at("tom") << endl;

	try {
		cout << contacts.at("toms") << endl;
	} catch (const exception& e) {
		cout << "not found" << endl;
	}

	return 0;
}