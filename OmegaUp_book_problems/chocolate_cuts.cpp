// OmegaUp book: page 31
#include "iostream"
#include "../utils/utils.hpp"

using namespace std;

int main(int argc, char const *argv[])
{
	int const MAX_PRICE = 1000;
	int columnsNum = atoi(argv[1]);
	int rowsNum = atoi(argv[2]);

	int verticalCutsNum = columnsNum - 1;
	int horizontalCutsNum = rowsNum - 1;

	cout << "verticalCutsNum: " << verticalCutsNum << endl;
	cout << "horizontalCutsNum: " << horizontalCutsNum << endl;

	int verticalPrices[MAX_PRICE + 1] = {0};
	int horizontalPrices[MAX_PRICE + 1] = {0};

	for (int i = 0, index = 3; i < verticalCutsNum; ++i, ++index)
	{
		verticalPrices[atoi(argv[index])]++;
	}

	for (int i = 0, index = 3 + verticalCutsNum;
		 i < horizontalCutsNum;
		 ++i, ++index)
	{
		horizontalPrices[atoi(argv[index])]++;
	}

	int madeVerticalCuts = 0;
	int madeHorizontalCuts = 0;
	int lowestPrice = 0;

	for (int i = MAX_PRICE; i >= 0; i--)
	{
		int price = i;

		if (verticalPrices[i] > 0)
		{
			lowestPrice += price * (madeHorizontalCuts + 1);
			verticalPrices[i]--;
			madeVerticalCuts++;
			// Force to return in this index until verticalPrices[i] == 0
			i++;
		}
		else if (horizontalPrices[i] > 0)
		{
			lowestPrice += price * (madeVerticalCuts + 1);
			horizontalPrices[i]--;
			madeHorizontalCuts++;
			i++;
		}
	}

	cout << lowestPrice << endl;

	return 0;
}