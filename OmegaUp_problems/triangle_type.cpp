#include <iostream>

int main(void)
{
	int side1, side2, side3;

	std::cin >> side1 >> side2 >> side3;

	int sumSides = side1 + side2 + side3;
	int maxSide = side1 < side2 ? side2 : side1;
	maxSide = maxSide < side3 ? side3 : maxSide;
	sumSides -= maxSide;

	if (maxSide >= sumSides || side1 <= 0 || side2 <= 0 || side3 <= 0)
		std::cout << "Invalid";
	else if (side1 == side2 && side1 == side3)
		std::cout << "Equilateral";
	else if (side1 == side2 || side1 == side3 || side2 == side3)
		std::cout << "Isosceles";
	else
		std::cout << "Scalene";

	return 0;
}