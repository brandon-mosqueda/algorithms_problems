// https://omegaup.com/arena/problem/Figuras-y-palitos-/#problems
#include <iostream>
#include "../utils/utils.hpp"

using namespace std;

enum Directions
{
	UP = (int)'R',
	LEFT = (int)'I',
	RIGHT = (int)'D',
	DOWN = (int)'B'
};

int getArea(char directions[], int N)
{
	int previousStepDirection = -1;
	int area = 0;

	for (int i = 0; i < N; ++i)
	{
		if (previousStepDirection == directions[i])
			area++;

		previousStepDirection = directions[i];
	}

	return area;
}

int main(int argc, char const *argv[])
{
	int nTestCases = atoi(argv[1]);
	int currentArgIndex = 2;

	for (int nTestCase = 0; nTestCase < nTestCases; ++nTestCase)
	{
		cout << "Case: " << nTestCase << endl;

		int N = atoi(argv[currentArgIndex++]);
		char directions[N];

		for (int i = 0; i < N; ++i)
			directions[i] = argv[currentArgIndex][i];

		currentArgIndex++;

		print(directions, N);

		cout << "Area: " << getArea(directions, N) << endl;

		cout << "*********" << endl << endl << endl;
	}

	return 0;
}